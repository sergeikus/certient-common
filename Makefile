.PHONY: test vendor

help:
	@echo "Available rules: "
	@echo "    Test rules:"
	@echo "        test - performs Go unit testing with coverage report"
	@echo
	@echo "    Other:"
	@echo "        clean - performs repository clean up, removes containers, images and temporary files"
	@echo "        vendor - download Go dependencies"
	@echo

test:
	@echo "==========   Performing '${server-project-name}' unit testing with coverage report   =========="
	@env CGO_ENABLED=0 go test -timeout 30s -coverprofile=c.out `go list ./... | grep -v /vendor/`
	@go tool cover -html=c.out -o coverage.html
	@echo "==========   Go coverage report is available in 'coverage.html'   =========="

vendor:
	go mod vendor
