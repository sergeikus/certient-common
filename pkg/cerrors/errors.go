package cerrors

import "fmt"

// MessageCode is small descriptive message sent to the client which may be used
// for translation
type MessageCode string

type ApiError struct {
	// Message is a visible message to client (end user)
	Message string `json:"message"`
	// Code is general specific code for translation
	Code MessageCode `json:"code"`
	// Status HTTP code
	Status int `json:"status"`
	// TrackID used for easier tracking in case of error
	TrackID string `json:"trackId"`
}

type Error struct {
	// InternalError will not be show or sent to the client
	InternalError error       `json:"internalError"`
	ClientMessage string      `json:"clientMessage"`
	MessageCode   MessageCode `json:"code"`
	LogInternal   bool        `json:"-"`
}

func (e *Error) WrapInternalError(err error) *Error {
	if e.InternalError == nil {
		e.InternalError = err
	} else {
		e.InternalError = fmt.Errorf("%v, %w", err, e.InternalError)
	}
	return e
}

func (e *Error) ForceLogInternal() *Error {
	e.LogInternal = true
	return e
}
