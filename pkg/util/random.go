package util

import (
	"crypto/rand"
	"fmt"
)

const uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
const lowercase = "abcdefghijklmnopqrstuvwxyz"
const letters = uppercase + lowercase
const numbers = "1234567890"
const mix = letters + numbers

// GenerateRandomString generates random string
func GenerateRandomString(length int) (string, error) {
	bs, err := randomBytes(length)
	if err != nil {
		return "", fmt.Errorf("failed to generate random bytes: %v", err)
	}
	for i, b := range bs {
		bs[i] = mix[b%byte(len(mix))]
	}
	return string(bs), nil
}

func randomBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	if err != nil {
		return nil, err
	}
	return b, nil
}
