package util

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func Test_GenerateRandomString(t *testing.T) {
	str, err := GenerateRandomString(10)
	require.NoErrorf(t, err, "expected to see no error for first string, but got: %v", err)

	str2, err := GenerateRandomString(10)
	require.NoErrorf(t, err, "expected to see no error for second string, but got: %v", err)

	require.NotEqual(t, str, str2, "expected to get different random strings, but got same")
}
